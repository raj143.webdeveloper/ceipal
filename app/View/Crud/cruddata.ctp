
<!Doctype html>
<html>

<head>
    
<script src="https://cdn.jsdelivr.net/jquery/1.12.4/jquery.min.js"></script>
 <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">
  
       <!-- for ajax cdn  -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.0/jquery.min.js"> </script>  
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"> </script>  

<script src="https://cdn.jsdelivr.net/jquery/1.12.4/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>




</head>

<body>


    <!-- signup page html code start -->

    <div class="container-fluid my-0 py-5 bg-info">
            <!-- <div class="d-flex flex-row justify-content-center "> -->

                <div class="col-12">

                 <?php  echo $this->Html->link('Addusers',['controller'=>'Crud','action'=>'Add_user'],['class'=>'btn btn-warning my-4']);
                        echo $this->Html->link('View States',['controller'=>'Crud','action'=>'states'],['class'=>'btn btn-primary my-4 ml-3']);
                    ?>           
                <table class="rounded" id="myTable">
                        <tr >
                            <th>ID</th>
                            <th>Picture</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Mobile</th>
                            <th>State</th>
                            <th>Languages</th>
                            <th>Gender</th>
                            <th>Created</th>
                            <!-- <th>Modified</th> -->
                            <th>Action</th>
                        </tr>

                        <tbody >
                            <?php
                            $i = 1;
                            // pr($data);exit;
                            foreach($data as $d){

                                $nm = $d['Crud']['name'];
                                $em = $d['Crud']['email'];
                                // $img = $d['Crud']['image'];
                                $mb = $d['Crud']['mobile'];
                                $st = $d['state']['name'];
                                $ln = $d['Crud']['language'];
                                $cr = $d['Crud']['created'];
                                // $md = $d['Crud']['modified'];
                                $id = $d['Crud']['uid'];
                                $gn = $d['Crud']['gender'];

                                echo "<tr class='bg-light'>";
                                echo "<td>$i</td>";
                                echo "<td>";
                                if(!empty($img)){
                                      echo $this->Html->image($img,['width'=>'50px','alt'=>'no pic']);
                                }else{
                                    echo $this->Html->image("no pic",['width'=>'20px','alt'=>'no pic']);
                                }
                                echo '</td>';
                                echo "<td>$nm</td>";
                                echo "<td>$em</td>";
                                echo "<td>$mb</td>";
                                echo "<td>$st</td>";
                                echo "<td>$ln</td>";
                                echo "<td>$gn</td>";
                                echo "<td>$cr</td>";
                            //    echo "<td>$md</td>";
                                echo "<td>";
                                    echo $this->Html->link('edit',['controller'=>'Crud','action'=>'Add_user',$id],['class'=>'btn btn-primary ml-3']);
                                    echo $this->Html->link('delete',['controller'=>'Crud','action'=>'deleteu',base64_encode($id)],['class'=>'btn btn-danger ml-3 mr-0','confirm'=>['are you sure you want to delete']]);
                                    
                                echo "<td>";
                                echo "</tr>";


                                $i += 1;
                            }




                                ?>
                        </tbody>
                </table>
                    

                </div>



            </div>

          </div>

        </div>



<script>

$(document).ready(function () {

    $('#myTable').DataTable();
        
        
});
       



</script>

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.12.1/css/jquery.dataTables.css">
<script type="text/javascript" charset="utf8"src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.js"></script>

      </body>
    <html>
    

